<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenarikanBarang extends Model
{
    public $primaryKey = 'id';
    protected $table = 'penarikan_barang_2';
    protected $fillable = ['kode_barang','nama_barang','tgl_expire','qty','status'];
}
