<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    public $primaryKey = 'id';
    protected $table = 'kategori';
    protected $fillable = ['nama_kategori'];
}
