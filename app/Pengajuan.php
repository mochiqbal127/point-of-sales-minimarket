<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengajuan extends Model
{
    public $primaryKey = 'id';
    protected $table = 'pengajuan_barang';
    protected $fillable = ['pelanggan_id','nama_barang','tgl_pengajuan','stok','status'];

    public function getStatusDisplayAttribute(){
        if (@$this->attributes['status']==1) return 'Terpenuhi';
        if (@$this->attributes['status']==0) return 'Belum Terpenuhi';
        return '-';
    }

    public function pelanggan()
    {
        return $this->belongsTo(Pelanggan::class, 'pelanggan_id');
    }
}
