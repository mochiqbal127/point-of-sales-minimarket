<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\PenarikanBarang;
class PenarikanExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return PenarikanBarang::all();
    }
}
