<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model
{
    public $primaryKey = 'id';
    protected $table = 'penjualan';
    protected $fillable = ['no_faktur','tanggal_faktur','total_bayar','pelanggan_id','user_id'];

    public function detailPenjualan()
    {
        return $this->hasMany(DetailPenjualan::class,'penjualan_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function pelanggan()
    {
        return $this->belongsTo(Pelanggan::class, 'pelanggan_id');
    }
    public function bayar()
    {
        return $this->hasOne(TampungBayar::class,'penjualan_id');
    }
}
