<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Pemasok;

class PemasokController extends Controller
{
    /**
     * Menampilkan list data Pemasok
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pemasok'] = Pemasok::all();
        return view('pemasok.index')->with($data);
    }


    /**
     * Menambah Data Baru pada Database
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'kode_pemasok' => 'required',
            'nama_pemasok' => 'required',
            'alamat' => 'required',
            'kota' => 'required',
            'no_telp' => 'required',
        ];
        $this->validate($request,$rules);

        $input = $request->all();
        $status = Pemasok::create($input);

        if($status) return redirect('/pemasok')->with('success','sukses');
        else return redirect('/pemasok')->with('failed','gagal');
    }


    /**
     * Mengubah Data pada Database
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'kode_pemasok' => 'required',
            'nama_pemasok' => 'required',
            'alamat' => 'required',
            'kota' => 'required',
            'no_telp' => 'required',
        ];
        $this->validate($request,$rules);

        $data = Pemasok::find($request->id);
        $data->kode_pemasok = $request->kode_pemasok;
        $data->nama_pemasok = $request->nama_pemasok;
        $data->alamat = $request->alamat;
        $data->kota = $request->kota;
        $data->no_telp = $request->no_telp;
        $status=$data->save();

        if($status) return redirect('/pemasok')->with('success','sukses');
        else return redirect('/pemasok')->with('failed','gagal');
    }

    /**
     * Menghapus Data pada Database
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = Pemasok::find($request->id);
        $status=$data->delete();

        if($status) return redirect('/pemasok')->with('success','sukses');
        else return redirect('/pemasok')->with('failed','gagal');
    }
}
