<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PenarikanBarang;
use App\Barang;
use App\Exports\PenarikanExport;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;

class PenarikanBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pb'] = PenarikanBarang::all();
        $data['barang'] = Barang::all();
        return view('penarikan/index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $rules=[
        //     'nama_kategori' => 'required',
        // ];
        // $this->validate($request, $rules);

        // $input = $request->all();
        // $status = PenarikanBarang::create($input);
        $data = new PenarikanBarang;
        $data->kode_barang = $request->kode_barang;
        $data->nama_barang = $request->nama_barang;
        $data->tgl_expire = $request->tgl_expire;
        $data->qty = $request->qty;
        $data->status = 0;
        $status=$data->save();

        if($status) return redirect('/penarikan')->with('success','Data Berhasil Disimpan!');
        else return redirect('/penarikan')->with('error','Data gagal Disimpan!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // $rules=[
        //     'nama_kategori' => 'required',
        // ];
        // $this->validate($request, $rules);

        $data = PenarikanBarang::find($request->id);
        $data->kode_barang = $request->kode_barang;
        $data->nama_barang = $request->nama_barang;
        $data->tgl_expire = $request->tgl_expire;
        $data->qty = $request->qty;
        $data->save();

        return redirect('/penarikan')->with('success','Data Berhasil Diubah!');
    }
    public function updateStatus(Request $request){
        // if($request->mode == "true")
        //     {
        //         $affected_products = DB::table('penarikan_barang')->where('id', '=', $request->comment_id)->update(array('status' => 1));
        //     }
        //     else
        //     {
        //         $affected_products = DB::table('penarikan_barang')->where('id', '=', $request->comment_id)->update(array('status' => 0));
        //     }
        $data = PenarikanBarang::find($request->id);
        $data->status = $request->status;
        $data->save();

        return response()->json(['success'=>'Status change successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = PenarikanBarang::find($request->id);
        $data->delete();

        return redirect('/penarikan')->with('success','Data Berhasil Dihapus!');
    }
    public function exportToExcel()
    {
        return Excel::download(new PenarikanExport,'penarikanbarang.xlsx');
    }
    public function exportToPdf(){
        $barang = PenarikanBarang::all();
        $pdf = PDF::loadView('penarikan.cetak',['penarikan'=>$barang]);
        return $pdf->stream('cetak-pdf-penarikan.pdf');
    }
    public function getNamaBarang(Request $request)
    {
        if ($request->ajax()) {
            $kode = $request->get('kode');
            $barang = Barang::where('kode_barang', $kode)->get();

            return response()->json([
                'message' => 'success',
                'barang' => $barang
            ]);
        }
    }
}
