<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengajuan;
use App\Pelanggan;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade as PDF;
use App\Exports\PengajuanExport;

class PengajuanController extends Controller
{
    // Menampilkan data dari database ke view
    public function index()
    {
        $data['pb'] = Pengajuan::all();
        $data['pelanggan'] = Pelanggan::all();
        return view('pengajuan/index')->with($data);
    }
    // Menambahkan data baru ke database
    public function store(Request $request)
    {
        $data = new Pengajuan();
        $data->pelanggan_id = $request->pelanggan_id;
        $data->nama_barang = $request->nama_barang;
        $data->tgl_pengajuan = $request->tgl_pengajuan;
        $data->stok = $request->stok;
        $data->status = 0;
        $status=$data->save();

        if($status) return redirect('/pengajuan')->with('success','Data Berhasil Disimpan!');
        else return redirect('/pengajuan')->with('error','Data gagal Disimpan!!');
    }

    // Mengubah data yang ada pada database
    public function update(Request $request)
    {
        $data = Pengajuan::find($request->id);
        $data->pelanggan_id = $request->pelanggan_id;
        $data->nama_barang = $request->nama_barang;
        $data->tgl_pengajuan = $request->tgl_pengajuan;
        $data->stok = $request->stok;
        $data->save();

        return redirect('/pengajuan')->with('success','Data Berhasil Diubah!');
    }

     // Mengubah kolum status sesuai id
    public function updateStatus(Request $request){
        $data = Pengajuan::find($request->id);
        $data->status = $request->status;
        $data->save();

        return response()->json(['success'=>'Status change successfully.']);
    }

     // Menghapus data yang ada pada database
    public function destroy(Request $request)
    {
        $data = Pengajuan::find($request->id);
        $data->delete();

        return redirect('/pengajuan')->with('success','Data Berhasil Dihapus!');
    }
    // Mengekspor pada file .xls
    public function exportToExcel()
    {
        return Excel::download(new PengajuanExport,'pengajuan.xlsx');
    }

    // Mengekspor pada file .pdf
    public function exportToPdf(){
        $barang = Pengajuan::all();
        $pdf = PDF::loadView('pengajuan.cetak',['pengajuan'=>$barang]);
        return $pdf->stream('cetak-pdf-pengajuan.pdf');
    }
    public function tambah(){
        $a = 1;
        $b = $a +$a;
        dd($b);
    }
}
