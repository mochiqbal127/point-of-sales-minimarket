<?php

namespace App\Http\Controllers;

use App\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    // Menampilkan data dari database ke view
    public function index()
    {
        $data['kategori'] = Kategori::all();
        return view('kategori/index')->with($data);
    }

    // Menambahkan data baru ke database
    public function store(Request $request)
    {
        $rules=[
            'nama_kategori' => 'required',
        ];
        $this->validate($request, $rules);

        $input = $request->all();
        $status = Kategori::create($input);

        if($status) return redirect('/kategori')->with('success','Data Berhasil Disimpan!');
        else return redirect('/kategori')->with('error','Data gagal Disimpan!!');
    }

    // Mengubah data yang ada pada database
    public function update(Request $request)
    {
        $rules=[
            'nama_kategori' => 'required',
        ];
        $this->validate($request, $rules);

        $data = Kategori::find($request->id);
        $data->nama_kategori = $request->nama_kategori;
        $data->save();

        return redirect('/kategori')->with('success','Data Berhasil Diubah!');
    }

    // Menghapus data yang ada pada database
    public function destroy(Request $request)
    {
        $data = Kategori::find($request->id);
        $data->delete();

        return redirect('/kategori')->with('success','Data Berhasil Dihapus!');
    }
}
