<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DetailPembelian;
use App\Pembelian;
use App\DetailPenjualan;
use App\Penjualan;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;


class FakturController extends Controller
{
    // Menampilkan data dari database ke view
    public function index_beli()
    {
        $data['pembelian'] = Pembelian::orderBy('created_at','DESC')->get();

        return view('faktur.beli')->with($data);
    }
    // Menampilkan data dari database ke view
    public function index_jual()
    {
        $data['penjualan'] = Penjualan::orderBy('created_at','DESC')->get();
        return view('faktur.jual')->with($data);
    }
    // Menampilkan data detail
    public function detail_beli($id)
    {
        $data['p'] = Pembelian::find($id);
        $data['dp'] = DetailPembelian::where('pembelian_id', $id)->get();
        return view('faktur.detailbeli')->with($data);
    }
    // Menampilkan data detail
    public function detail_jual($id)
    {
        $data['p'] = Penjualan::find($id);
        $data['dp'] = DetailPenjualan::where('penjualan_id', $id)->get();
        return view('faktur.detailjual')->with($data);
    }
    // Menampilkan tampilan faktur
    public function faktur_beli($id)
    {
        $p = Pembelian::find($id);
        $dp = DetailPembelian::where('pembelian_id', $id)->get();
        $pdf = PDF::loadView('pembelian.faktur',['dp'=>$dp,'p'=>$p]);
        return $pdf->stream('cetak-faktur-pembelian.pdf');
    }
    // Menampilkan tampilan faktur
    public function faktur_jual($id)
    {
        $p = Penjualan::find($id);
        $dp = DetailPenjualan::where('penjualan_id', $id)->get();
        $pdf = PDF::loadView('penjualan.faktur',['dp'=>$dp,'p'=>$p]);
        return $pdf->stream('cetak-faktur-penjualan.pdf');
    }
}
