<?php

namespace App\Http\Controllers;

use App\Penjualan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    // Menampilkan view
    public function index()
    {
        return view('index');
    }
    // Menampilkan data dari database ke view
    function data_penjualan($lastCount){
        if($lastCount == 0){
            $data = Penjualan::select('tanggal_faktur',
                DB::raw('SUM(total_bayar) as total_bayar'),
                DB::raw('COUNT(id) as jml_transaksi'))
                ->groupBy('tanggal_faktur')
                ->orderBy('tanggal_faktur','asc')
                ->get();
        }else{
            $data = Penjualan::select('tanggal_faktur',
                DB::raw('SUM(total_bayar) as total_bayar'),
                DB::raw('COUNT(id) as jml_transaksi'))
                ->groupBy('tanggal_faktur')
                ->orderBy('tanggal_faktur','asc')
                ->skip($lastCount-1)
                ->limit(32434324324)
                ->get();
        }

        return response($data);
    }
}
