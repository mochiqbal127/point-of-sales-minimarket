<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoggingController extends Controller
{
    public function index(){
        $data['data'] = \App\Logging::all();
        return view('logging.index')->with($data);
    }
}
