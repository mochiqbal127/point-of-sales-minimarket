<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Barang;
use App\Exports\BarangExport;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;

class BarangController extends Controller
{
    // Menampilkan data dari database ke view
    public function index()
    {
        $data['barang'] = Barang::all();
        return view('barang.index')->with($data);
    }

    // Menginput data baru ke database
    public function store(Request $request)
    {
        $rules=[
            'kode_barang' => 'required',
            'produk_id' => 'required',
            'nama_barang' => 'required',
            'satuan' => 'required',
            'harga_jual' => 'required',
            'stok' => 'required',
        ];
        $this->validate($request, $rules);

        $input = $request->all();
        $status = Barang::create($input);

        if($status) return redirect('/barang')->with('success','Data Berhasil Disimpan!');
        else return redirect('/barang')->with('error','Data gagal Disimpan!!');
    }

    // Mengekspor ke file .xls
    public function exportToExcel()
    {
        return Excel::download(new BarangExport,'barang.xlsx');
    }

    // Mengekspor ke file .pdf
    public function exportToPdf(){
        $barang = Barang::all();

        $pdf = PDF::loadView('barang.cetak',['barang'=>$barang]);
        return $pdf->stream('cetak-pdf-barang.pdf');
    }

    // Mengubah data yang ada pada database
    public function update(Request $request)
    {
        $rules=[
            'kode_barang' => 'required',
            'produk_id' => 'required',
            'nama_barang' => 'required',
            'satuan' => 'required',
            'harga_jual' => 'required',
            'stok' => 'required',
        ];
        $this->validate($request, $rules);

        $data = Barang::find($request->id);
        $data->kode_barang = $request->kode_barang;
        $data->produk_id = $request->produk_id;
        $data->nama_barang = $request->nama_barang;
        $data->satuan = $request->satuan;
        $data->harga_jual = $request->harga_jual;
        $data->stok = $request->stok;
        $status= $data->save();

        if($status) return redirect('/barang')->with('success','Data Berhasil Disimpan!');
        else return redirect('/barang')->with('error','Data gagal Disimpan!!');
    }

    // Menghapus data yang ada pada database
    public function destroy(Request $request)
    {
        $data = Barang::find($request->id);
        $data->delete();

        return redirect('/barang')->with('success','Data Berhasil Dihapus!');
    }
}
