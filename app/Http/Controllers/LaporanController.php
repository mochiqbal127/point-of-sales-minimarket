<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Penjualan;
use App\Pembelian;

class LaporanController extends Controller
{
    public function indexbeli(){
        $data['p'] = Pembelian::all();
        return view('laporan.beli')->with($data);
    }
    public function indexjual(){
        $data['p'] = Penjualan::all();
        return view('laporan.jual')->with($data);
    }
}
