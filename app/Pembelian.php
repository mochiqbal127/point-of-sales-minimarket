<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembelian extends Model
{
    public $primaryKey = 'id';
    protected $table = 'pembelian';
    protected $fillable = ['kode_masuk','tanggal_masuk','total','pemasok_id','user_id'];

    public function pemasok()
    {
        return $this->belongsTo(Pemasok::class, 'pemasok_id');
    }
    public function detailPembelian()
    {
        return $this->hasMany(DetailPembelian::class,'pembelian_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

}
