<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logging extends Model
{
    public $primaryKey = 'id';
    protected $table = 'logging';
}
