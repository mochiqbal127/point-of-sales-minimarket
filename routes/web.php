<?php
Route::auth();

Route::middleware(['middleware'=> 'auth'])->group(function () {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index');
    Route::get('data_penjualan/{lastCount}', 'HomeController@data_penjualan');
    Route::resource('kategori', 'KategoriBarangController');
    Route::resource('pemasok', 'PemasokController');
    Route::resource('barang', 'BarangController');
    Route::resource('produk', 'ProdukController');
    Route::resource('pelanggan', 'PelangganController');

    Route::put('produk','ProdukController@update');
    Route::delete('produk','ProdukController@destroy');

    Route::get('/penarikan', 'PenarikanBarangController@index')->name('penarikan');
    Route::post('/penarikan', 'PenarikanBarangController@store');
    Route::get('penarikan/updateStatus', 'PenarikanBarangController@updateStatus')->name('updateStatus');
    Route::put('/penarikan', 'PenarikanBarangController@update');
    Route::delete('/penarikan', 'PenarikanBarangController@destroy');
    Route::get('penarikan/export/xls','PenarikanBarangController@exportToExcel');
    Route::get('penarikan/export/pdf','PenarikanBarangController@exportToPdf');
    Route::get('get-nama-barang','PenarikanBarangController@getNamaBarang')->name('getNamaBarang');

    Route::put('barang','BarangController@update');
    Route::delete('barang','BarangController@destroy');
    Route::get('barang/export/xls','BarangController@exportToExcel');
    Route::get('barang/export/pdf','BarangController@exportToPdf');

    Route::put('pemasok','PemasokController@update');
    Route::delete('pemasok','PemasokController@destroy');

    Route::put('pelanggan','PelangganController@update');
    Route::delete('pelanggan','PelangganController@destroy');

    Route::get('/pembelian','PembelianController@index')->name('pembelian.index');
    Route::post('/pembelian','PembelianController@store');

    Route::get('/penjualan','PenjualanController@index')->name('penjualan.index');
    Route::post('/penjualan','PenjualanController@store');

    Route::get('/faktur/beli','FakturController@index_beli');
    Route::get('/faktur/beli/{id}','FakturController@detail_beli');
    Route::get('/pembelian/{id}','FakturController@faktur_beli');
    Route::get('/faktur/jual','FakturController@index_jual');
    Route::get('/faktur/jual/{id}','FakturController@detail_jual');
    Route::get('/penjualan/{id}','FakturController@faktur_jual');

    Route::get('/report/jual','LaporanController@indexjual');
    Route::get('/report/beli','LaporanController@indexbeli');

    Route::get('/pengajuan', 'PengajuanController@index')->name('pengajuan');
    Route::post('/pengajuan', 'PengajuanController@store');
    Route::put('/pengajuan', 'PengajuanController@update');
    Route::delete('/pengajuan', 'PengajuanController@destroy');
    Route::get('pengajuan/updateStatus', 'PengajuanController@updateStatus')->name('ubahStatus');
    Route::get('pengajuan/export/xls','PengajuanController@exportToExcel');
    Route::get('pengajuan/export/pdf','PengajuanController@exportToPdf');
    Route::get('tambah','PengajuanController@tambah');

    Route::get('/logging', 'LoggingController@index')->name('logging');

});

