<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePenarikanBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penarikan_barang', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_barang',10);
            $table->string('nama_barang',100);
            $table->date('tgl_expire');
            $table->bigInteger('qty');
            $table->string('status',15);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penarikan_barang');
    }
}
