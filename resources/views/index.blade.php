@extends('tmplt.header')
@push('style')

@endpush
@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
    <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
  </div>

  <!-- Content Row -->
  <div class="row">

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">User Petugas</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{\App\User::count()}}</div>
            </div>
            <div class="col-auto">
              <i class="fas fa-user fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Jumlah Pelanggan</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{\App\Pelanggan::count()}}</div>
            </div>
            <div class="col-auto">
              <i class="fas fa-user fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Jumlah Barang</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{\App\Barang::count()}}</div>
            </div>
            <div class="col-auto">
              <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Pending Requests Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-warning shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Jumlah Produk</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{\App\Produk::count()}}</div>
            </div>
            <div class="col-auto">
              <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-4 col-md-6 mb-4">
      <div class="card border-left-danger shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Jumlah Pembelian</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{\App\Pembelian::count()}}</div>
            </div>
            <div class="col-auto">
              <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Barang Ditarik</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">{{\App\PenarikanBarang::where('status','1')->count()}}</div>
              </div>
              <div class="col-auto">
                <i class="fas fa-truck-pickup fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
        </div>
      </div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-4 col-md-6 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Jumlah Penjualan</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{\App\Penjualan::count()}}</div>
            </div>
            <div class="col-auto">
              <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Content Row -->

  <div class="row">

    <!-- Area Chart -->
    <div class="col-xl-12 col-lg-12">
      <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 class="m-0 font-weight-bold text-primary">Grafik Pendapatan</h6>
        </div>
        <!-- Card Body -->
        <div class="card-body">
          <div class="chart-area" id="chartContainer">
          </div>
        </div>
      </div>
    </div>

  </div>


@endsection
@push('script')
<script>
window.onload = function () {
    var dataPenjualan = [];
    var dataJmlTransaksi = [];

    var chart;

    $.get("{{ url('data_penjualan')}}/0", function(data){
        $.each(data, function(key,value){
            let date = value['tanggal_faktur'];
            let year = date.substring(0,4);
            let month = date.substring(5,7)-1;
            let day = date.substring(8,10);
            // console.log(year+"-"+month+"-"+day);
            dataPenjualan.push({
                x: new Date(year, month, day),
                y: parseInt(value['total_bayar'])
            });

             dataJmlTransaksi.push({
                x: new Date(year, month, day),
                y: parseInt(value['jml_transaksi'])
            });
        });

        chart = new CanvasJS.Chart("chartContainer", {
        title:{
            text: "Grafik Pendapatan"
        },
        axisY:{
            title: "Penjualan",
            lineColor: "#C24642",
            tickColor: "#C24642",
            labelFontColor: "#C24642",
            titleFontColor: "#C24642",
            includeZero: true,
            suffix: ""
        },
        axisY2: {
            title: "Pendapatan",
            lineColor: "#7F6084",
            tickColor: "#7F6084",
            labelFontColor: "#7F6084",
            titleFontColor: "#7F6084",
            includeZero: true,
            prefix: "",
            suffix: ""
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeries
        },
        data: [
        {
            type: "line",
            name: "Penjualan",
            color: "#C24642",
            axisYIndex: 0,
            showInLegend: true,
            dataPoints: dataJmlTransaksi
        },
        {
            type: "line",
            name: "Pendapatan",
            color: "#7F6084",
            axisYType: "secondary",
            showInLegend: true,
            dataPoints: dataPenjualan
        }]
    });
    chart.render();
    updateChart();
});


    function updateChart(){
        console.log(dataJmlTransaksi);
        $.get("{{ url('data_penjualan')}}/"+dataJmlTransaksi.length, function(data){
            $.each(data, function(key,value){
                let date = value['tanggal_faktur'];
                let year = date.substring(0,4);
                let month = date.substring(5,7)-1;
                let day = date.substring(8,10);
                console.log(year+"-"+month+"-"+day);
                dataPenjualan.push({
                    x: new Date(year, month, day),
                    y: parseInt(value['total_bayar'])
                });

                if(dataPenjualan.length == 1)
                    dataJmlTransaksi.pop({
                            x: new Date(year, month, day),
                            y: parseInt(value['jml_transaksi'])
                        });
                else
                    dataJmlTransaksi.push({
                        x: new Date(year, month, day),
                        y: parseInt(value['jml_transaksi'])
                    });

            });
            });
        chart.render();
        setTimeout(function(){updateChart()},10000);
    }

    function toggleDataSeries(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chart.render();
    }

}
</script>
@endpush
