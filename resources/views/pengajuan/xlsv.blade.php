<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Pengaju</th>
            <th>Nama Barang</th>
            <th>Tanggal Pengajuan</th>
            <th>Stok</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $row2)
        <tr>
            <td>{{ !empty($i) ? ++$i : $i = 1 }}</th>
            <td>{{ $row2->pelanggan->nama }}</td>
            <td>{{ $row2->nama_barang }}</td>
            <td>{{ $row2->tgl_pengajuan}}</td>
            <td>{{ $row2->stok }}</td>
            <td>{{ $row2->status_display }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
</body>
</html>
