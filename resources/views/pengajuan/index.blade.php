@extends('tmplt.header')
@push('style')

@endpush
@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-900">Pengajuan Barang</h1>
</div>
@include('tmplt.feedback')
<div id="message"></div>
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        {{-- <h6 class="h5 m-0 font-weight-bold text-gray-100">Pengajuan Barang</h6> --}}
        <div>
            <a href="#add" data-toggle="modal" data-target="#formModal" data-mode="add" class="btn btn-sm btn-secondary shadow-sm"><i class="fa fa-plus"></i> Tambah Pengajuan</a>
        </div>
        <div class="d-sm-inline-block">
            <button id="export-pdf" class="btn btn-sm btn-danger shadow-sm"><i class="fa fa-print"></i> Export PDF</button>
            <button id="export-xls" class="btn btn-sm btn-success shadow-sm"><i class="fa fa-print"></i> Export Xls</button>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table text-gray-800" id="dtprod" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Pengaju</th>
                    <th>Nama Barang</th>
                    <th>Tanggal Pengajuan</th>
                    <th>Stok</th>
                    <th>Terpenuhi?</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($pb as $row)
                    <tr>
                        <td>{{ !empty($i) ? ++$i : $i = 1 }}</th>
                        <td>{{ $row->pelanggan->nama }}</td>
                        <td>{{ $row->nama_barang }}</td>
                        <td>{{ $row->tgl_pengajuan }}</td>
                        <td>{{ $row->stok }}</td>
                        <td>
                            <div class="custom-control custom-switch">
                                <input id="switch-primary-{{$row->id}}" class="toggle-class custom-control-input" data-id="{{$row->id}}" value="{{$row->id}}" name="toggle"  type="checkbox" {{ $row->status == 1 ? 'checked' : '' }}>
                                <label class="custom-control-label" for="switch-primary-{{$row->id}}" ></label>
                            </div>
                        </td>
                        <td>
                            <a href="#" data-toggle="modal" data-target="#formModal" data-mode="edit"
                            data-id="{{$row->id}}"
                            data-kode="{{$row->pelanggan_id}}"
                            data-nama="{{$row->nama_barang}}"
                            data-tgl="{{$row->tgl_pengajuan}}"
                            data-qty="{{$row->stok}}"
                            class="btn btn-sm btn-warning"><i class='fa fa-edit'></i></a>
                            <a href="#dlt" data-toggle="modal" data-target="#deleteModal"
                            data-id="{{$row->id}}"
                            data-nama="{{$row->nama_barang}}"
                            class="btn btn-sm btn-danger"><i class='fa fa-trash'></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
        </table>

        </div>
    </div>
</div>
@endsection
@include('pengajuan.form')
@push('script')
<script>
    $(function(){
        $('#dtprod').DataTable();
        $('.select2').select2();
        $('#export-xls').on('click', function(e){
            window.location = '{{url("pengajuan/export/xls")}}';
        });
        $('#export-pdf').on('click', function(e){
            window.location = '{{url("pengajuan/export/pdf")}}';
        });
        $('#formModal').on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var mode = button.data('mode');
            var nama = button.data('nama');
            var kode = button.data('kode');
            var tgl = button.data('tgl');
            var qty = button.data('qty');
            var modal = $(this);
            if(mode=='edit'){
                modal.find('.modal-title').text('Edit Data Pengajuan Barang');
                modal.find('.modal-body #inputKode').val(kode);
                modal.find('.modal-body #inputKode').trigger('change');
                modal.find('.modal-body #inputNama').val(nama);
                modal.find('.modal-body #inputStok').val(qty);
                modal.find('.modal-body #inputTgl').val(tgl);
                modal.find('.modal-body #method').html('{{method_field("PUT")}}<input type="hidden" id="id" name="id" value="'+id+'">');

            }
            else{
                modal.find('.modal-title').text('Tambah Data Pengajuan Barang');
                modal.find('.modal-body #inputProduk').val('');
                modal.find('.modal-body #inputKode').val('');
                modal.find('.modal-body #inputNama').val('');
                modal.find('.modal-body #inputStok').val('');
                modal.find('.modal-body #inputTgl').val('');
                modal.find('.modal-body #method').html('');
            }
        });
        $('#deleteModal').on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var produk = button.data('nama');
            var modal = $(this);
            modal.find('.modal-body #dataHapus').html(produk);
            modal.find('.modal-body #idHapus').val(id);
        });
        $('.toggle-class').change(function() {
            var status = $(this).prop('checked') == true ? 1 : 0;
            var id = $(this).data('id');

            $.ajax({
                type: "GET",
                dataType: "json",
                url: '{{route('ubahStatus')}}',
                data: {'status': status, 'id': id},
                success: function(data){
                console.log(data.success),
                $('#message').append('<div class="alert with-close alert-success alert-dismissible"><button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Berhasil update status</div>')
            }
        });
    })

    })
</script>
@endpush
