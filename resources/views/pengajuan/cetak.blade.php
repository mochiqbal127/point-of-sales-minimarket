<html lang="en">
<head>

    <title>Cetak</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="{{asset('assets')}}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> --}}
    <link href="{{asset('assets')}}/css/sb-admin-2.min.css" rel="stylesheet">
</head>
<body>
	<center>
		<h5>Data Barang</h5>
	</center>
    <div class="">
        <div class="">
            <div class="">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama Pengaju</th>
                        <th>Nama Barang</th>
                        <th>Tanggal Pengajuan</th>
                        <th>Stok</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($pengajuan as $row2)
                        <tr>
                            <td>{{ !empty($i) ? ++$i : $i = 1 }}</th>
                            <td>{{ $row2->pelanggan->nama }}</td>
                            <td>{{ $row2->nama_barang }}</td>
                            <td>{{ $row2->tgl_pengajuan}}</td>
                            <td>{{ $row2->stok }}</td>
                            <td>{{ $row2->status_display }}</td>
                        </tr>
                        @endforeach
                    </tbody>
            </table>
            </div>
        </div>
    </div>
</body>
</html>
