<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header bg-primary text-gray-100">
            <h5 class="modal-title" id="myModalLabel">Tambah Data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ url('pengajuan')}}" method="POST">
                {{ csrf_field() }}
                <div id="method"></div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Pelanggan
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <select id="inputKode" class="form-control select2" name="pelanggan_id" style="height: 100;width: 100%;">
                            @foreach (\App\Pelanggan::all() as $row)
                            <option value="{{ $row->id}}">
                            {{ $row->nama}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Barang<span class="required">*</span>
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="inputNama" name="nama_barang" required="required" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Pengajuan<span class="required">*</span>
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="date" id="inputTgl" name="tgl_pengajuan" required="required" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Stok<span class="required">*</span>
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="inputStok" name="stok" required="required" class="form-control">
                    </div>
                </div>
                <div class="ln_solid"></div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
        </div>
    </div>
</div>
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header bg-danger text-gray-100">
            <h5 class="modal-title" id="exampleModalLabel">Hapus Data Pengajuan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ url('pengajuan')}}" method="POST">
                {{ csrf_field() }}
                {{ method_field('delete') }}

                <div class="form-group">
                    <input type="hidden" name="id" id="idHapus">
                    Apakah data <b id="dataHapus"></b> ini akan dihapus?
                </div>
        </div>
        <div class="modal-footer">
            <button class="btn" type="button" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-danger">Delete</button>
        </div>
        </form>
        </div>
    </div>
</div>
