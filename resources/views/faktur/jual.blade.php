@extends('tmplt.header')
@push('style')

@endpush
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Data Penjualan</h6>
        {{-- <a href="#add" data-toggle="modal" data-target="#formModal" data-mode="add" class="d-sm-inline-block btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Tambah</a> --}}
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table text-gray-800" id="dtprod" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>#</th>
                    <th>No. Faktur</th>
                    <th>Tanggal</th>
                    <th>Pelanggan</th>
                    <th>Total Bayar</th>
                    <th>Operator</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($penjualan as $row)
                    <tr>
                        <td>{{ !empty($i) ? ++$i : $i = 1 }}</th>
                        <td>{{ $row->no_faktur }}</td>
                        <td>{{ $row->tanggal_faktur }}</td>
                        <td>{{ $row->pelanggan->nama }}</td>
                        <td>{{ $row->total_bayar }}</td>
                        <td>{{ $row->user->name }}</td>
                        <td>
                            <a href="{{url("faktur/jual/$row->id")}}" class="btn btn-sm btn-info"><i class='fa fa-eye'></i></a>
                            <a href="{{url("penjualan/$row->id")}}" class="btn btn-sm btn-danger"><i class='fa fa-print'></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
        </table>

        </div>
    </div>
</div>
@endsection
@push('script')

@endpush
