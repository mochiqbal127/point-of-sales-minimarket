@extends('tmplt.header')
@push('style')

@endpush
@section('content')
@include('tmplt.feedback')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Detail Pembelian</h6>
        <div class="d-inline-block">
            {{-- <a href="#add" data-toggle="modal" data-target="#formModal" data-mode="add" class="btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Tambah</a>
            <button href="#add" data-toggle="modal" data-target="#cetakModal" class="btn btn-sm btn-secondary shadow-sm"><i class="fa fa-print"></i> Export</button> --}}
            <small class="text-gray-300">{{$p->no_faktur}}</small>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table table-striped text-gray-800" width="100%" cellspacing="0">
            <thead>
                <tr>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="4">No. Faktur</td>
                    <td>:</td>
                    <td>{{$p->no_faktur}}</td>
                </tr>
                <tr>
                    <td colspan="4">Tanggal Faktur</td>
                    <td>:</td>
                    <td>{{$p->tanggal_faktur}}</td>
                </tr>
                <tr>
                    <td colspan="4">Nama Pelanggan</td>
                    <td>:</td>
                    <td>{{$p->pelanggan->nama}}</td>
                </tr>
                <tr>
                    <td colspan="4">Nama Operator</td>
                    <td>:</td>
                    <td>{{$p->user->name}}</td>
                </tr>
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
            </tbody>
        </table>

        </div>
    </div>
</div>
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">List Barang</h6>
        <div class="d-inline-block">

        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table text-gray-800" id="dtprod" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>#</th>
                <th>Kode</th>
                <th>Barang</th>
                <th>Harga Beli</th>
                <th>QTY</th>
                <th>Subtotal</th>
            </tr>
            </thead>
            @php
                $totalakhir = 0;
            @endphp
            <tbody>
                @foreach ($dp as $row2)
                <tr>
                    <td>{{ !empty($i) ? ++$i : $i = 1 }}</th>
                    <td>{{ $row2->barang->kode_barang }}</td>
                    <td>{{ $row2->barang->nama_barang }}</td>
                    <td>{{ $row2->harga_jual}}</td>
                    <td>{{ $row2->jumlah }}</td>
                    <td>{{ $row2->sub_total }}</td>
                </tr>
                @php
                    $totalakhir += $row2->sub_total;
                @endphp
                @endforeach
            </tbody>
            <tfoot class="">
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                    <td><strong>Total</strong></td>
                    <td>:</td>
                    <td>{{$totalakhir}}</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                    <td><strong>Bayar</strong></td>
                    <td>:</td>
                    <td>{{$p->bayar->terima}}</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                    <td><strong>Kembali</strong></td>
                    <td>:</td>
                    <td>{{$p->bayar->kembali}}</td>
                </tr>
            </tfoot>
        </table>
        </div>
    </div>
</div>
<div class="d-sm-flex align-items-center justify-content-between">
    <div></div>
    <div class="d-inline-block">
        <a href="{{url("penjualan/$p->id")}}" class="btn btn-primary">Cetak Faktur</a>
    </div>
</div>
<div>
    <br>
</div>

@endsection
{{-- @include('barang.form') --}}
@push('script')

@endpush
