@extends('tmplt.header')
@push('style')

@endpush
@section('content')
@include('tmplt.feedback')
<div id="message"></div>
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Data Penarikan Barang</h6>
        <div class="d-sm-inline-block">
            <a href="#add" data-toggle="modal" data-target="#formModal" data-mode="add" class="btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Tambah</a>
            <button href="#add" data-toggle="modal" data-target="#cetakModal" class="btn btn-sm btn-secondary shadow-sm"><i class="fa fa-print"></i> Export</button>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table text-gray-800" id="dtprod" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Tanggal Expire</th>
                    <th>QTY</th>
                    <th>Ditarik?</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($pb as $row)
                    <tr>
                        <td>{{ !empty($i) ? ++$i : $i = 1 }}</th>
                        <td>{{ $row->kode_barang }}</td>
                        <td>{{ $row->nama_barang }}</td>
                        <td>{{ $row->tgl_expire }}</td>
                        <td>{{ $row->qty }}</td>
                        <td>
                            <div class="custom-control custom-switch">
                                <input id="switch-primary-{{$row->id}}" class="toggle-class custom-control-input" data-id="{{$row->id}}" value="{{$row->id}}" name="toggle"  type="checkbox" {{ $row->status == 1 ? 'checked' : '' }}>
                                <label class="custom-control-label" for="switch-primary-{{$row->id}}" ></label>
                            </div>
                        </td>
                        <td>
                            <a href="#" data-toggle="modal" data-target="#formModal" data-mode="edit"
                            data-id="{{$row->id}}"
                            data-kode="{{$row->kode_barang}}"
                            data-nama="{{$row->nama_barang}}"
                            data-tgl="{{$row->tgl_expire}}"
                            data-qty="{{$row->qty}}"
                            class="btn btn-sm btn-warning"><i class='fa fa-edit'></i></a>
                            <a href="#dlt" data-toggle="modal" data-target="#deleteModal"
                            data-id="{{$row->id}}"
                            data-nama="{{$row->nama_barang}}"
                            class="btn btn-sm btn-danger"><i class='fa fa-trash'></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
        </table>

        </div>
    </div>
</div>
@endsection
@include('penarikan.form')
@push('script')
<script>
    $(function(){
        $('#dtprod').DataTable();
        $('.select2').select2();
        $('#btn-excel').on('click', function(e){
            window.location = '{{url("penarikan/export/xls")}}';
        });
        $('#btn-pdf').on('click', function(e){
            window.location = '{{url("penarikan/export/pdf")}}';
        });
        $('#formModal').on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var mode = button.data('mode');
            var nama = button.data('nama');
            var kode = button.data('kode');
            var tgl = button.data('tgl');
            var qty = button.data('qty');
            var modal = $(this);
            if(mode=='edit'){
                modal.find('.modal-title').text('Edit Data Penarikan Barang');
                modal.find('.modal-body #inputKode').val(kode);
                modal.find('.modal-body #inputNama').val(nama);
                modal.find('.modal-body #inputQty').val(qty);
                modal.find('.modal-body #inputTgl').val(tgl);
                modal.find('.modal-body #method').html('{{method_field("PUT")}}<input type="hidden" id="id" name="id" value="'+id+'">');

            }
            else{
                modal.find('.modal-title').text('Tambah Data Penarikan Barang');
                modal.find('.modal-body #inputProduk').val('');
                modal.find('.modal-body #inputKode').val('');
                modal.find('.modal-body #inputNama').val('');
                modal.find('.modal-body #inputQty').val('');
                modal.find('.modal-body #inputTgl').val('');
                modal.find('.modal-body #method').html('');
            }
        });
        $('#deleteModal').on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var produk = button.data('nama');
            var modal = $(this);
            modal.find('.modal-body #dataHapus').html(produk);
            modal.find('.modal-body #idHapus').val(id);
        });
        // $('input[name=toggle]').change(function(){
        //     var mode= $(this).prop('checked');
        //     var id=$( this ).val();

        //         var productObj = {};
        //         productObj.mode = $(this).prop('checked');
        //         productObj.comment_id = $( this ).val();
        //         productObj._token = '{{csrf_token()}}';

        //     $.ajax({
        //     type:"POST",
        //     dataType:"JSON",
        //     url:"{{ url('penarikan/updateStatus') }}",
        //     data:productObj,
        //     success:function(data)
        //     {
        //     }
        //     });
        // });
        $('.toggle-class').change(function() {
        var status = $(this).prop('checked') == true ? 1 : 0;
        var id = $(this).data('id');

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '{{route('updateStatus')}}',
            data: {'status': status, 'id': id},
            success: function(data){
              console.log(data.success),
              $('#message').append('<div class="alert with-close alert-success alert-dismissible"><button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Berhasil update status</div>')
            }
        });
    })

    })
</script>
@endpush
