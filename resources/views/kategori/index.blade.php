@extends('tmplt.header')
@section('content')
@include('tmplt.feedback')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Data Kategori</h6>
        <a href="#add" data-toggle="modal" data-target="#formModal" data-mode="add" class="d-sm-inline-block btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Tambah</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table text-gray-800" id="dtprod" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Kategori</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($kategori as $row)
                    <tr>
                        <td>{{ !empty($i) ? ++$i : $i = 1 }}</th>
                        <td>{{ $row->nama_kategori }}</td>
                        <td>
                            <a href="#" data-toggle="modal" data-target="#formModal" data-mode="edit"
                            data-id="{{$row->id}}"
                            data-nama="{{$row->nama_kategori}}"
                            class="btn btn-sm btn-warning"><i class='fa fa-edit'></i></a>
                            <a href="#dlt" data-toggle="modal" data-target="#deleteModal"
                            data-id="{{$row->id}}"
                            data-nama="{{$row->nama_kategori}}"
                            class="btn btn-sm btn-danger"><i class='fa fa-trash'></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
        </table>

        </div>
    </div>
</div>
@endsection
@include('kategori.form')
@push('script')
<script>
    $(function(){
        $('#dtprod').DataTable();
        $('#formModal').on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var mode = button.data('mode');
            var produk = button.data('nama');
            var modal = $(this);
            if(mode=='edit'){
                modal.find('.modal-title').text('Edit Data Kategori');
                modal.find('.modal-body #inputProduk').val(produk);
                modal.find('.modal-body #method').html('{{method_field("PUT")}}<input type="hidden" id="id" name="id" value="'+id+'">');

            }
            else{
                modal.find('.modal-title').text('Tambah Data Kategori');
                modal.find('.modal-body #inputProduk').val('');
                modal.find('.modal-body #method').html('');
            }
        });
        $('#deleteModal').on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var produk = button.data('nama');
            var modal = $(this);
            modal.find('.modal-body #dataHapus').html(produk);
            modal.find('.modal-body #idHapus').val(id);
        });
    })
</script>
@endpush
