<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="{{asset('assets')}}/css/bootstrap.min.css" rel="stylesheet">
    <!------ Include the above in your HEAD tag ---------->
</head>
<body>

        <div class="card">
          <div class="card-header">
            Invoice
            <strong>{{$p->kode_masuk}}</strong>
            <span class="float-right"> <strong>{{$p->tanggal_masuk}}</strong></span>
          </div>
          <div class="card-body">
            <div>
                <br><br>
            </div>
            <div class="mb-4">
              {{-- <div class="col-sm-6">
                <div>
                  <strong>Nama Pelanggan :{{$p->pelanggan->nama}}</strong>
                </div>
                <div>Attn: Daniel Marek</div>
                <div>43-190 Mikolow, Poland</div>
                <div>Email: marek@daniel.com</div>
                <div>Phone: +48 123 456 789</div>
              </div> --}}
              <div>
                <table>
                    <tbody>
                        <tr>
                            <td>Kode Pemasok </td>
                            <td> :</td>
                            <td> {{$p->pemasok->kode_pemasok}}</td>
                        </tr>
                        <tr>
                            <td>Nama Pemasok </td>
                            <td> :</td>
                            <td> {{$p->pemasok->nama_pemasok}}</td>
                        </tr>
                        <tr>
                            <td>Alamat </td>
                            <td> :</td>
                            <td> {{$p->pemasok->alamat}}</td>
                        </tr>
                        <tr>
                            <td>Kota </td>
                            <td> :</td>
                            <td> {{$p->pemasok->kota}}</td>
                        </tr>
                        <tr>
                            <td>No. Telepon </td>
                            <td> :</td>
                            <td> {{$p->pemasok->no_telp}}</td>
                        </tr>
                    </tbody>
                </table>
              </div>
            </div>

            <div class="table-responsive-sm">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Kode Barang</th>
                    <th>Barang</th>
                    <th>Harga Beli</th>
                    <th>QTY</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                @php
                    $totalakhir = 0;
                @endphp
                <tbody>
                    @foreach ($dp as $row)
                    <tr>
                        <td>{{ !empty($i) ? ++$i : $i = 1 }}</td>
                        <td>{{$row->barang->kode_barang}}</td>
                        <td>{{$row->barang->nama_barang}}</td>
                        <td>{{$row->harga_beli}}</td>
                        <td>{{$row->jumlah}}</td>
                        <td>{{$row->total}}</td>
                        @php
                            $totalakhir += $row->total;
                        @endphp
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <div class="row">
              <div class="col-lg-4 col-sm-5">

              </div>

              <div class="col-lg-4 col-sm-5 ml-auto">
                <table class="table table-clear">
                  <tbody>
                    <tr>
                      <td class="left">
                        <strong>Total</strong>
                      </td>
                      <td class="right">Rp. {{$totalakhir}}</td>
                    </tr>
                  </tbody>
                </table>

              </div>

            </div>

          </div>
        </div>
      </div>
</body>
</html>

