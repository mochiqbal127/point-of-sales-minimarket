@extends('tmplt.header')
@push('style')

@endpush
@section('content')

<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Laporan Pembelian</h6>
        <div class="d-inline-block">
            <button href="#add" data-toggle="modal" data-target="#cetakModal" class="btn btn-sm btn-secondary shadow-sm"><i class="fa fa-print"></i> Cetak Laporan</button>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table text-gray-800" id="dtprod" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>#</th>
                <th>No. Faktur</th>
                <th>Tanggal</th>
                <th>Total</th>
                <th>Pelanggan</th>
                <th>Operator</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($p as $row2)
                <tr>
                    <td>{{ !empty($i) ? ++$i : $i = 1 }}</th>
                    <td>{{ $row2->no_faktur}}</td>
                    <td>{{ $row2->tanggal_faktur }}</td>
                    <td>{{ $row2->total_bayar }}</td>
                    <td>{{ $row2->pelanggan->nama }}</td>
                    <td>{{ $row2->user->name }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>

        </div>
    </div>
</div>

@endsection

@push('script')
<script>

</script>
@endpush
