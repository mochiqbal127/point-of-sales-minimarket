
<!-- -->
<!-- Bootstrap core JavaScript-->
<script src="{{asset('assets')}}/vendor/jquery/jquery.min.js"></script>
<script src="{{asset('assets')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="{{asset('assets')}}/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="{{asset('assets')}}/js/sb-admin-2.min.js"></script>
<!-- Datatables-->

<script src="{{asset('assets')}}/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('assets')}}/vendor/datatables/dataTables.bootstrap4.min.js"></script>
{{-- <script src="{{asset('assets')}}/vendor/chart.js/Chart.min.js"></script> --}}

  <!-- Page level custom scripts -->
<script src="{{asset('js')}}/canvasjs.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
{{-- <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script> --}}
@stack('script')
</body>

</html>
